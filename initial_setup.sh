
CURR_PATH=${PWD}

# make sure you adjust these if you are going to test locally
WS_PATH="/workspace"
REPOS_PATH="${WS_PATH}/src/_lib"

cd ${REPOS_PATH}/turtlebot3_msgs; git checkout melodic-devel;
cd ${REPOS_PATH}/turtlebot3; git checkout melodic-devel;
cd ${REPOS_PATH}/turtlebot3_simulations; git checkout melodic-devel; git apply ${CURR_PATH}/turtlebot3_simulations.patch;
cd ${REPOS_PATH}/turtlebot3_manipulation; git checkout melodic-devel;
cd ${REPOS_PATH}/turtlebot3_manipulation_simulations; git checkout melodic-devel; git apply ${CURR_PATH}/turtlebot3_manipulation_simulations.patch;
cd ${REPOS_PATH}/open_manipulator; git checkout melodic-devel;
cd ${REPOS_PATH}/robotis_manipulator; git checkout melodic-devel;
cd ${REPOS_PATH}/open_manipulator_msgs; git checkout melodic-devel;
cd ${REPOS_PATH}/open_manipulator_simulations; git checkout melodic-devel;
cd ${REPOS_PATH}/open_manipulator_dependencies; git checkout melodic-devel;
cd ${REPOS_PATH}/robotis_manipulator; git checkout melodic-devel;
cd ${REPOS_PATH}/DynamixelSDK; git checkout melodic-devel;
cd ${REPOS_PATH}/dynamixel-workbench; git checkout melodic-devel;
cd ${REPOS_PATH}/dynamixel-workbench-msgs; git checkout melodic-devel;
cd ${REPOS_PATH}/ar_track_alvar; git checkout melodic-devel;

sudo apt update -y;
sudo apt install -y ros-melodic-moveit-msgs;
sudo apt install -y ros-melodic-moveit-core \
                    ros-melodic-moveit-ros-planning \
                    ros-melodic-ar-track-alvar-msgs \
                    ros-melodic-moveit-ros-planning-interface \
                    ros-melodic-effort-controllers;

cd ${WS_PATH}
catkin_make;

source ${WS_PATH}/devel/setup.bash;
